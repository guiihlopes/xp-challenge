import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./Header'),
  loading: Loading
});

export default class LoadableHeader extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
