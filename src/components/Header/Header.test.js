import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('Header', () => {
  const HeaderComponent = shallow(<Header />);

  it('renders correctly', () => {
    expect(HeaderComponent).toMatchSnapshot();
  });
});
