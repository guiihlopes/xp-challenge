import React from 'react';
import { Link } from 'react-router-dom';
import HeaderStyle from './Header.css';
import logo from 'static/logo.png';

class Header extends React.Component {
  render() {
    return (
      <header className={HeaderStyle.header}>
        <h1>
          <Link to="/" title="Spotify logo">
            <img src={logo} alt="Spotify logo" />
          </Link>
        </h1>
      </header>
    );
  }
}

export default Header;
