import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./AlbumCover'),
  loading: Loading
});

export default class LoadableAlbumCover extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
