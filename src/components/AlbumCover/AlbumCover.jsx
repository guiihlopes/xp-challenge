import React from 'react';
import PropTypes from 'prop-types';
import idx from 'idx';
import AlbumCoverStyle from './AlbumCover.css';

class AlbumsListItem extends React.Component {
  getArtistsName = () => {
    const { album } = this.props;

    return idx(album, _ => _.artists)
      .map(artist => {
        return artist.name;
      })
      .join(', ');
  };

  getAlbumImage = () => {
    const { album } = this.props;

    return idx(album, _ => _.images[1].url);
  };

  render() {
    const { album } = this.props;
    return (
      <div className={AlbumCoverStyle.albumCover}>
        <figure>
          <img src={this.getAlbumImage()} alt={album.name} />
        </figure>
        <div className="album-info">
          <h5 className={AlbumCoverStyle.albumName}>{album.name}</h5>
          <span className={AlbumCoverStyle.albumArtists}>
            {this.getArtistsName()}
          </span>
        </div>
      </div>
    );
  }
}

AlbumsListItem.propTypes = {
  album: PropTypes.object.isRequired
};

export default AlbumsListItem;
