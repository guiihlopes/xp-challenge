import React from 'react';
import { shallow } from 'enzyme';
import AlbumCover from './AlbumCover';

describe('AlbumCover', () => {
  const albumProp = {
    name: 'U2',
    artists: [{ name: 'Bono Vox' }],
    images: [{ url: 'http://teste.com.br/foto.png' }]
  };
  const AlbumCoverComponent = shallow(<AlbumCover album={albumProp} />);

  it('renders correctly', () => {
    expect(AlbumCoverComponent).toMatchSnapshot();
  });
});
