import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./Modal'),
  loading: Loading
});

export default class LoadableModal extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
