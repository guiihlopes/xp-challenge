import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

// const modalRoot = document.getElementById('modal-root');

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
    this.modalRoot =
      document.getElementById('modal-root') || document.querySelector('body');
  }

  componentDidMount() {
    this.modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    this.modalRoot.removeChild(this.el);
  }

  render() {
    const { children } = this.props;
    return ReactDOM.createPortal(children, this.el);
  }
}

Modal.propTypes = {
  children: PropTypes.element.isRequired
};

export default Modal;
