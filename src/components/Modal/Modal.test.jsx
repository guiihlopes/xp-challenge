import React from 'react';
import { shallow } from 'enzyme';
import Modal from './Modal';

// document.getElementById('modal-root') = document.createElement('div');

describe('Modal', () => {
  const ModalComponent = shallow(
    <Modal>
      <div>Test</div>
    </Modal>
  );

  it('renders correctly', () => {
    expect(ModalComponent).toMatchSnapshot();
  });
});
