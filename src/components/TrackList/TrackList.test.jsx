import React from 'react';
import { shallow } from 'enzyme';
import TrackList from './TrackList';

describe('TrackList', () => {
  const trackProp = {
    id: 46546,
    track_number: 1,
    duration_ms: 300000,
    name: 'U2',
    preview_url: 'http://teste.com.br/foto.png'
  };
  const tracksProp = [trackProp, trackProp, trackProp];
  const TrackListComponent = shallow(<TrackList tracks={tracksProp} />);

  it('renders correctly', () => {
    expect(TrackListComponent).toMatchSnapshot();
  });
});
