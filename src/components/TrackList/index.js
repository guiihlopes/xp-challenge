import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./TrackList'),
  loading: Loading
});

export default class LoadableTrackList extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
