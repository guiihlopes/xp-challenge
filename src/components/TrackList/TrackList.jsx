import React from 'react';
import PropTypes from 'prop-types';
import TrackListItem from 'components/TrackListItem';
import TrackListStyle from './TrackList.css';

class TrackList extends React.Component {
  renderItens = () => {
    const { tracks } = this.props;
    return tracks.map((track, index) => {
      return <TrackListItem track={track} key={index} />;
    });
  };

  render() {
    return (
      <ol className={TrackListStyle.tracks}>
        <div className={TrackListStyle.trackList}>{this.renderItens()}</div>
      </ol>
    );
  }
}

TrackList.propTypes = {
  tracks: PropTypes.array.isRequired
};

export default TrackList;
