import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./AccessTokenHandler'),
  loading: Loading
});

export default class LoadableAccessTokenHandler extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
