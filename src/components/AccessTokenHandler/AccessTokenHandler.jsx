import React from 'react';
import PropTypes from 'prop-types';
import AccessTokenHandlerStyle from './AccessTokenHandler.css';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { validate } from 'store/modules/auth';

import Cookies from 'js-cookie';

export class AccessTokenHandler extends React.Component {
  state = {
    inputToken: ''
  };

  handleSubmit = ev => {
    ev.preventDefault();
    const { inputToken } = this.state;
    const { validate } = this.props;
    Cookies.set(process.env.AUTH_COOKIE, inputToken);
    validate();
    window.location.reload();
  };

  handleInputTokenChange = ev => {
    this.setState({
      inputToken: ev.target.value
    });
  };

  render() {
    const { inputToken, open } = this.state;
    return (
      <div
        className={`${AccessTokenHandlerStyle.backdrop} ${
          open ? AccessTokenHandlerStyle.active : ''
        }`}
      >
        <div id={AccessTokenHandlerStyle.modal}>
          <div className={AccessTokenHandlerStyle.modalContent}>
            <h3>
              Oooops! Digite um token válido para iniciar ou renovar sua sessão
              :D
            </h3>
            <form onSubmit={this.handleSubmit}>
              <div className={AccessTokenHandlerStyle.inputGroup}>
                <label htmlFor="accessToken">Seu token de acesso</label>
                <input
                  type="text"
                  id="accessToken"
                  value={inputToken}
                  onChange={this.handleInputTokenChange}
                  required
                />
              </div>
              <div className={AccessTokenHandlerStyle.formHandlers}>
                <button type="submit">Inserir token!</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

AccessTokenHandler.propTypes = {
  validate: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  validate: () => dispatch(validate())
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(AccessTokenHandler)
);
