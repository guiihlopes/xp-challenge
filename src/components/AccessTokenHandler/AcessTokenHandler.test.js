import React from 'react';
import { shallow } from 'enzyme';
import { AccessTokenHandler } from './AccessTokenHandler';
import Cookies from 'js-cookie';

// https://github.com/jsdom/jsdom/issues/2112
delete window.location;
window.location = { reload: () => {} };

describe('AccessTokenHandler', () => {
  const AccessTokenHandlerComponent = shallow(
    <AccessTokenHandler validate={() => {}} />
  );

  it('renders correctly', () => {
    expect(AccessTokenHandlerComponent).toMatchSnapshot();
  });

  it('initialize the `state` with an empty input token', () => {
    expect(AccessTokenHandlerComponent.state().inputToken).toEqual('');
  });

  describe('when the user insert a token in the input', () => {
    const token = '2jfioewjfioewj31';

    beforeEach(() => {
      AccessTokenHandlerComponent.find('#accessToken').simulate('change', {
        target: { value: token }
      });
    });

    it('updates the local inputToken in `state`', () => {
      expect(AccessTokenHandlerComponent.state().inputToken).toEqual(token);
    });
  });

  describe('when the user submit a form', () => {
    const token = '2jfioewjfioewj31';

    beforeEach(() => {
      AccessTokenHandlerComponent.find('#accessToken').simulate('change', {
        target: { value: token }
      });
    });

    it('sets a new cookie with the inputToken', () => {
      const form = AccessTokenHandlerComponent.find('form');
      form.simulate('submit', { preventDefault() {} });
      const cookieValue = Object.values(
        Cookies.get(process.env.AUTH_COOKIE)
      )[0];
      expect(cookieValue).toEqual(token);
    });
  });
});
