import React from 'react';
import PropTypes from 'prop-types';
import AlbumsListItem from 'components/AlbumsListItem';
import AlbumsListStyle from './AlbumsList.css';

class AlbumsList extends React.Component {
  renderItens = () => {
    const { albums } = this.props;
    return albums.map((album, index) => {
      return <AlbumsListItem album={album} key={index} />;
    });
  };
  render() {
    return (
      <ul className={AlbumsListStyle.albums}>
        <div className={AlbumsListStyle.albumList}>{this.renderItens()}</div>
      </ul>
    );
  }
}

AlbumsList.propTypes = {
  albums: PropTypes.array.isRequired
};

export default AlbumsList;
