import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./AlbumsList'),
  loading: Loading
});

export default class LoadableAlbumsList extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
