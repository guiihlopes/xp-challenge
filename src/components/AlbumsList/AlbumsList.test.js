import React from 'react';
import { shallow } from 'enzyme';
import AlbumsList from './AlbumsList';

describe('AlbumsList', () => {
  const albumProp = {
    name: 'U2',
    artists: [{ name: 'Bono Vox' }],
    images: [{ url: 'http://teste.com.br/foto.png' }]
  };
  const albumsProp = [albumProp, albumProp, albumProp];
  const AlbumsListComponent = shallow(<AlbumsList albums={albumsProp} />);

  it('renders correctly', () => {
    expect(AlbumsListComponent).toMatchSnapshot();
  });
});
