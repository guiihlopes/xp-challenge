import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';
import idx from 'idx';
import classnames from 'classnames';
import { msToTime } from 'helpers/Time';
import { getTrack } from 'services/spotify/Track';
import { invalidate } from 'store/modules/auth';
import { play, pause } from 'store/modules/player';
import TrackListItemStyle from './TrackListItem.css';

export class TrackListItem extends React.Component {
  _isMounted = false;
  state = {
    auth: true,
    previewUrl: null,
    audio: null
  };

  componentDidMount = () => {
    this._isMounted = true;
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  handlePlay = ev => {
    ev.preventDefault();
    const { track, authGlobalState } = this.props;
    const { audio } = this.state;
    if (audio) return this.playAudio();
    const token = Cookies.get(process.env.AUTH_COOKIE);
    if (authGlobalState.auth) {
      getTrack(token, track.id)
        .then(response => {
          if (response.status >= 200 && response.status < 300) {
            if (this._isMounted) {
              const previewUrl = idx(response, _ => _.data.preview_url);
              if (previewUrl) {
                this.setState(
                  {
                    auth: true,
                    previewUrl: previewUrl
                  },
                  () => {
                    this.playAudio();
                  }
                );
              } else {
                this.setState({
                  auth: true,
                  previewUrl: ''
                });
              }
            }
          }
        })
        .catch(() => {
          this.props.invalidate();
          if (this._isMounted) {
            this.setState({
              auth: false
            });
          }
        });
    }
  };

  playAudio = () => {
    const { previewUrl, audio: storedAudio } = this.state;
    const { track, play } = this.props;
    if (!previewUrl) return false;
    if (storedAudio) {
      play(storedAudio, track.id);
    } else {
      const audio = new Audio(previewUrl);
      play(audio, track.id);
      this.setState({
        audio
      });
    }
  };

  pauseAudio = ev => {
    ev.preventDefault();
    const { pause } = this.props;
    pause();
  };

  render() {
    const { track, actualTrack } = this.props;
    const { previewUrl, auth } = this.state;

    const audioPaused = idx(actualTrack, _ => _.audio.paused);
    const playing = !audioPaused && actualTrack.trackId == track.id;

    return (
      <li
        className={classnames(
          TrackListItemStyle.lisItem,
          playing ? TrackListItemStyle.active : ''
        )}
      >
        <span className={TrackListItemStyle.trackNumber}>{`${
          track.track_number
        }.`}</span>
        <a
          href="#"
          onClick={
            previewUrl !== ''
              ? playing
                ? this.pauseAudio
                : this.handlePlay
              : ev => ev.preventDefault()
          }
          id="playButton"
          className={TrackListItemStyle.playButton}
        >
          {previewUrl !== '' ? (
            playing ? (
              <svg className="icon-pause" viewBox="0 0 60 100">
                <path
                  fill="currentColor"
                  d="M0 8c0-5 3-8 8-8s9 3 9 8v84c0 5-4 8-9 8s-8-3-8-8V8zm43 0c0-5 3-8 8-8s8 3 8 8v84c0 5-3 8-8 8s-8-3-8-8V8z"
                >
                  <title>PAUSAR</title>
                </path>
              </svg>
            ) : (
              <svg className="icon-play" viewBox="0 0 85 100">
                <path
                  fill="currentColor"
                  d="M81 44.6c5 3 5 7.8 0 10.8L9 98.7c-5 3-9 .7-9-5V6.3c0-5.7 4-8 9-5l72 43.3z"
                >
                  <title>PLAY</title>
                </path>
              </svg>
            )
          ) : (
            <h5>Sem preview :(</h5>
          )}
        </a>
        <div className={TrackListItemStyle.trackNameWrapper}>
          <span className={TrackListItemStyle.trackName}>
            {auth ? track.name : 'Por favor insira um token válido :('}
          </span>
        </div>
        <span className={TrackListItemStyle.trackDuration}>
          {msToTime(track.duration_ms)}
        </span>
      </li>
    );
  }
}

TrackListItem.propTypes = {
  track: PropTypes.object.isRequired,
  actualTrack: PropTypes.object.isRequired,
  authGlobalState: PropTypes.object.isRequired,
  play: PropTypes.func.isRequired,
  pause: PropTypes.func.isRequired,
  invalidate: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  authGlobalState: state.auth,
  actualTrack: state.player
});

const mapDispatchToProps = dispatch => ({
  invalidate: () => dispatch(invalidate()),
  play: (audio, trackId) => dispatch(play(audio, trackId)),
  pause: () => dispatch(pause())
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TrackListItem)
);
