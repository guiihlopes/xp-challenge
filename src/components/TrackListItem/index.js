import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./TrackListItem'),
  loading: Loading
});

export default class LoadableTrackListItem extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
