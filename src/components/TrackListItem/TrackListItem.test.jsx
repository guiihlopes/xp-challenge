import React from 'react';
import { shallow } from 'enzyme';
import { TrackListItem } from './TrackListItem';

Object.defineProperty(window.HTMLMediaElement.prototype, 'paused', {
  writable: true,
  value: {}
});

describe('TrackListItem', () => {
  const trackProp = {
    id: 46546,
    track_number: 1,
    duration_ms: 300000,
    name: 'U2',
    preview_url: 'http://teste.com.br/foto.png'
  };

  const TrackListItemComponent = shallow(
    <TrackListItem
      track={trackProp}
      authGlobalState={{ auth: true }}
      actualTrack={{
        audio: new Audio(),
        trackId: 'fsdfds'
      }}
      invalidate={() => {}}
      play={() => {}}
      pause={() => {}}
    />
  );

  it('renders correctly', () => {
    expect(TrackListItemComponent).toMatchSnapshot();
  });

  it('initialize the `state` with auth true, previewUrll null and audio null', () => {
    const initialState = {
      auth: true,
      previewUrl: null,
      audio: null
    };

    const state = TrackListItemComponent.state();
    expect(state.auth).toEqual(initialState.auth);
    expect(state.previewUrl).toEqual(initialState.previewUrl);
    expect(state.audio).toEqual(initialState.audio);
  });

  describe('when clicking on play/pause button', () => {
    it('should call nothing when dont have previewUrl is an empty string', () => {
      TrackListItemComponent.setState({ previewUrl: '' });
      const instance = TrackListItemComponent.instance();
      const spy = jest.spyOn(instance, 'handlePlay');
      TrackListItemComponent.update();
      TrackListItemComponent.find('#playButton').simulate('click', {
        preventDefault() {}
      });
      expect(spy).toHaveBeenCalledTimes(0);
    });
    it('should call handlePlay having previewUrl', () => {
      const spy = jest.spyOn(TrackListItemComponent.instance(), 'handlePlay');
      TrackListItemComponent.setState({ previewUrl: 'http://teste.com' });
      TrackListItemComponent.update();
      TrackListItemComponent.find('#playButton').simulate('click', {
        preventDefault() {}
      });
      expect(spy).toHaveBeenCalled();
    });
    it('should call pauseAudio when previewUrl is not true, is the same trackId and audio is running', () => {
      const instance = TrackListItemComponent.instance();
      const spy = jest.spyOn(instance, 'pauseAudio');
      TrackListItemComponent.setProps({
        actualTrack: {
          audio: new Audio(),
          trackId: trackProp.id
        }
      });
      instance.props.actualTrack.audio.paused = false;
      TrackListItemComponent.setState({ previewUrl: 'http://teste.com' });
      TrackListItemComponent.update();
      TrackListItemComponent.find('#playButton').simulate('click', {
        preventDefault() {}
      });
      expect(spy).toHaveBeenCalled();
    });
  });
});
