import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./AlbumsListItem'),
  loading: Loading
});

export default class LoadableAlbumsListItem extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
