import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import AlbumCover from 'components/AlbumCover';
import AlbumsListItemStyle from './AlbumsListItem.css';

class AlbumsListItem extends React.Component {
  render() {
    const { album } = this.props;
    return (
      <li className={classnames(AlbumsListItemStyle.lisItem, 'column-5')}>
        <Link to={`/album/${album.id}`}>
          <AlbumCover album={album} />
        </Link>
      </li>
    );
  }
}

AlbumsListItem.propTypes = {
  album: PropTypes.object.isRequired
};

export default AlbumsListItem;
