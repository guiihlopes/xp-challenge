import React from 'react';
import { shallow } from 'enzyme';
import AlbumsListItem from './AlbumsListItem';

describe('AlbumsListItem', () => {
  const albumProp = {
    id: 'jfwhj2432',
    name: 'U2',
    artists: [{ name: 'Bono Vox' }],
    images: [{ url: 'http://teste.com.br/foto.png' }]
  };

  const AlbumsListItemComponent = shallow(<AlbumsListItem album={albumProp} />);

  it('renders correctly', () => {
    expect(AlbumsListItemComponent).toMatchSnapshot();
  });
});
