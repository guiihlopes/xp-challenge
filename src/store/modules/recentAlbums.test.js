import reducer, { types, addAlbumToStore } from './recentAlbums';

describe('recentAlbums store', () => {
  const album = {
    id: 'fjfiodsj',
    name: 'Testing my albums'
  };
  describe('recentAlbums actions', () => {
    it('should add and an album to store', () => {
      const expectedAction = {
        type: types.ADD,
        payload: { [album.id]: album }
      };
      expect(addAlbumToStore(album)).toEqual(expectedAction);
    });
  });

  describe('recentAlbums reducer', () => {
    const initialState = {
      albums: {}
    };
    it('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual(initialState);
    });
    it('should handle ADD', () => {
      expect(
        reducer(initialState, {
          type: types.ADD,
          payload: { [album.id]: album }
        })
      ).toEqual({
        albums: { ...initialState.albums, ...{ [album.id]: album } }
      });
    });
  });
});
