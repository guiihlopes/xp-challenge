import recentAlbums from './recentAlbums';
import searchedAlbums from './searchedAlbums';
import auth from './auth';
import player from './player';

export { recentAlbums, auth, searchedAlbums, player };
