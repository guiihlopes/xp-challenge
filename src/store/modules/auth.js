export const types = {
  NOT_VALID: 'spotify/auth/NOT_VALID',
  VALID: 'spotify/auth/VALID'
};

const initialState = { auth: true };

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.NOT_VALID:
      return { auth: false };
    case types.VALID:
      return { auth: true };
    default:
      return state;
  }
}

export function validate() {
  return {
    type: types.VALID
  };
}
export function invalidate() {
  return {
    type: types.NOT_VALID
  };
}
