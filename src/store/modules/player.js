export const types = {
  PLAY: 'spotify/player/PLAY',
  PAUSE: 'spotify/player/PAUSE'
};

const initialState = {
  audio: new Audio(),
  trackId: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.PLAY:
      if (state.audio instanceof Audio && !state.audio.paused) {
        state.audio.pause();
      }
      action.payload.audio.play();
      return {
        ...action.payload
      };
    case types.PAUSE:
      if (state.audio instanceof Audio && !state.audio.paused) {
        state.audio.pause();
      }
      return {
        ...state
      };
    default:
      return state;
  }
}

export function play(audio, trackId) {
  return {
    type: types.PLAY,
    payload: { audio, trackId }
  };
}

export function pause() {
  return {
    type: types.PAUSE
  };
}
