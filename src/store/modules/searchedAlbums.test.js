import reducer, { types, addAlbumsBySlug } from './searchedAlbums';

describe('searchedAlbums store', () => {
  const slug = 'mc-kevinho';
  const albums = [
    {
      id: 'fjfiodsj',
      name: 'Testing my albums'
    },
    {
      id: 'fjfiodsj',
      name: 'Testing my albums'
    }
  ];
  describe('searchedAlbums actions', () => {
    it('should add and an album to store', () => {
      const expectedAction = {
        type: types.ADD,
        payload: { [slug]: albums }
      };
      expect(addAlbumsBySlug(slug, albums)).toEqual(expectedAction);
    });
  });

  describe('searchedAlbums reducer', () => {
    const initialState = {
      albums: {}
    };
    it('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual(initialState);
    });
    it('should handle ADD', () => {
      expect(
        reducer(initialState, {
          type: types.ADD,
          payload: { [slug]: albums }
        })
      ).toEqual({
        albums: { ...initialState.albums, ...{ [slug]: albums } }
      });
    });
  });
});
