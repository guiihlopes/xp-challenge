export const types = {
  ADD: 'spotify/recentsAlbums/ADD'
};

const initialState = {
  albums: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ADD:
      return {
        albums: { ...state.albums, ...action.payload }
      };
    default:
      return state;
  }
}

export function addAlbumToStore(album) {
  return {
    type: types.ADD,
    payload: { [album.id]: album }
  };
}
