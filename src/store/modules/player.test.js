import reducer, { types, play, pause } from './player';

// https://github.com/jsdom/jsdom/issues/2155
window.HTMLMediaElement.prototype.play = () => {
  /* do nothing */
};
window.HTMLMediaElement.prototype.pause = () => {
  /* do nothing */
};

describe('player store', () => {
  const audio = new Audio();
  const trackId = '456sdff56sd';
  describe('player actions', () => {
    it('should play an audio', () => {
      const expectedAction = {
        type: types.PLAY,
        payload: { audio, trackId }
      };
      expect(play(audio, trackId)).toEqual(expectedAction);
    });
    it('should pause an audio', () => {
      const expectedAction = {
        type: types.PAUSE
      };
      expect(pause()).toEqual(expectedAction);
    });
  });

  describe('player reducer', () => {
    const initialState = {
      audio: new Audio(),
      trackId: null
    };
    it('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual(initialState);
    });
    it('should handle PLAY', () => {
      expect(
        reducer(initialState, {
          type: types.PLAY,
          payload: { audio, trackId }
        })
      ).toEqual({ audio, trackId });
    });
    it('should handle PAUSE', () => {
      expect(
        reducer(initialState, {
          type: types.PAUSE
        })
      ).toEqual(initialState);
    });
  });
});
