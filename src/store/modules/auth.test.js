import reducer, { types, validate, invalidate } from './auth';

describe('auth store', () => {
  describe('auth actions', () => {
    it('should be an valid auth', () => {
      const expectedAction = {
        type: types.VALID
      };
      expect(validate()).toEqual(expectedAction);
    });
    it('should be an invvalid auth', () => {
      const expectedAction = {
        type: types.NOT_VALID
      };
      expect(invalidate()).toEqual(expectedAction);
    });
  });

  describe('auth reducer', () => {
    const initialState = { auth: true };
    it('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual(initialState);
    });
    it('should handle VALID', () => {
      expect(
        reducer(initialState, {
          type: types.VALID
        })
      ).toEqual({ auth: true });
    });
    it('should handle NOT_VALID', () => {
      expect(
        reducer(initialState, {
          type: types.NOT_VALID
        })
      ).toEqual({ auth: false });
    });
  });
});
