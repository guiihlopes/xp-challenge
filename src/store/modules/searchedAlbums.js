export const types = {
  ADD: 'spotify/searchedAlbums/ADD'
};

const initialState = {
  albums: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ADD:
      return {
        albums: { ...state.albums, ...action.payload }
      };
    default:
      return state;
  }
}

export function addAlbumsBySlug(slug, albums) {
  return {
    type: types.ADD,
    payload: { [slug]: albums }
  };
}
