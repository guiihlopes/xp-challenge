import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import NotFoundRoute from 'pages/NotFoundRoute';
import CoreLayout from 'layouts/CoreLayout';
import RawLayout from 'layouts/RawLayout';

const withRawLayout = ['/album/:artist'];
const withCoreLayout = ['/', '/albums/:query'];

const RootRouter = ({ store, persistor }) => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <Switch>
          {withRawLayout.map((path, index) => (
            <Route
              exact
              path={path}
              key={`${path}-${index}`}
              component={RawLayout}
            />
          ))}
          {withCoreLayout.map((path, index) => (
            <Route
              exact
              path={path}
              key={`${path}-${index}`}
              component={CoreLayout}
            />
          ))}
          <Route component={NotFoundRoute} />
        </Switch>
      </Router>
    </PersistGate>
  </Provider>
);

RootRouter.propTypes = {
  store: PropTypes.object.isRequired,
  persistor: PropTypes.object.isRequired
};

export default RootRouter;
