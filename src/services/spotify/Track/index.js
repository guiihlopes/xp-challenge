import { defaultService } from 'services/commom';

export const getTrack = (token, id) =>
  defaultService(`Bearer ${token}`).get(`/tracks/${id}`);
