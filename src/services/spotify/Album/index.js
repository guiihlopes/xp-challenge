import { defaultService } from 'services/commom';

export const getAlbum = (token, id) =>
  defaultService(`Bearer ${token}`).get(`/albums/${id}`);
