import { defaultService } from 'services/commom';

export const search = (token, query, type = 'album,artist,track', limit = 10) =>
  defaultService(`Bearer ${token}`).get(
    `/search?q=${query}&type=${type}&limit=${limit}`
  );
