import axios from 'axios';

const http = (endpoint, token, contentType, timeoutCustom = null) => {
  const headers = {
    'Content-Type': contentType || 'application/json'
  };
  if (token) {
    headers.Authorization = token;
    headers.Accept = 'application/json';
  }
  return axios.create({
    baseURL: endpoint,
    timeout: timeoutCustom || 5000,
    headers
  });
};

export const defaultService = (token = null) =>
  http(process.env.SPOTIFY_URL, token, 'application/json');

export default http;
