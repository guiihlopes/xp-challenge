import moment from 'moment';

const toHuman = time => (time < 10 ? `0${time}` : time);

export const msToTime = ms => {
  const duration = moment.duration(ms, 'ms');
  const minutes = Math.floor(duration.asMinutes());
  const seconds = toHuman(
    Math.floor(duration.subtract(minutes, 'm').asSeconds())
  );

  return `${minutes}:${seconds}`;
};
