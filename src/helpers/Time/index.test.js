import { msToTime } from './index';

describe('Time methods', () => {
  const givenMs = 300000;
  const expectedTime = '5:00';
  describe('msToTime', () => {
    it('givenMs "3600" to be "5:00"', () => {
      expect(msToTime(givenMs)).toBe(expectedTime);
    });
  });
});
