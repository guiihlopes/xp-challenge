import { stringToSlug, slugToString } from './index';

describe('Url methods', () => {
  const slug = 'michael-jackson-jr';
  const stringNormalized = 'michael jackson jr';
  const uniqueString = 'michael';
  const emptyString = '';
  describe('stringToSlug', () => {
    it('string "michael jackson jr" to be "michael-jackson-jr"', () => {
      expect(stringToSlug(stringNormalized)).toBe(slug);
    });

    it('string "michael" to be "michael"', () => {
      expect(stringToSlug(uniqueString)).toBe(uniqueString);
    });

    it('string "" to be ""', () => {
      expect(stringToSlug(emptyString)).toBe(emptyString);
    });
  });

  describe('slugToString', () => {
    it('string "michael-jackson-jr" to be "michael jackson jr"', () => {
      expect(slugToString(slug)).toBe(stringNormalized);
    });

    it('string "michael" to be "michael"', () => {
      expect(slugToString(uniqueString)).toBe(uniqueString);
    });

    it('string "" to be ""', () => {
      expect(stringToSlug(emptyString)).toBe(emptyString);
    });
  });
});
