const stringToSlug = string => string.replace(/ /g, '-');

const slugToString = slug => slug.replace(/-/g, ' ');

export { stringToSlug, slugToString };
