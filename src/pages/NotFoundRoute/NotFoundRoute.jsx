import React from 'react';
import NotFoundRoute from './NotFoundRoute.css';

export default () => (
  <div>
    <p className={NotFoundRoute.notFoundTitle}>404! Page not found :( </p>
  </div>
);
