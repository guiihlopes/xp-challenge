import React from 'react';
import { shallow } from 'enzyme';
import NotFoundRoute from './NotFoundRoute';

describe('NotFoundRoute', () => {
  const NotFoundRouteComponent = shallow(<NotFoundRoute />);

  it('renders correctly', () => {
    expect(NotFoundRouteComponent).toMatchSnapshot();
  });
});
