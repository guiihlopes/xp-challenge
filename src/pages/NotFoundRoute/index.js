import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./NotFoundRoute'),
  loading: Loading
});

export default class LoadableNotFoundRoute extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
