import React from 'react';
import { shallow } from 'enzyme';
import Albums from './Albums';

describe('Albums', () => {
  const AlbumsComponent = shallow(<Albums />);

  it('renders correctly', () => {
    expect(AlbumsComponent).toMatchSnapshot();
  });
});
