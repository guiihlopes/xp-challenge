import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./Albums'),
  loading: Loading
});

export default class LoadableAlbums extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
