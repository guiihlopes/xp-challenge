import React from 'react';
import PropTypes from 'prop-types';
import idx from 'idx';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';
import AlbumsStyle from './Albums.css';
import AlbumsList from 'components/AlbumsList';
import { addAlbumsBySlug } from 'store/modules/searchedAlbums';
import { invalidate } from 'store/modules/auth';
import { search } from 'services/spotify/Search';

class Albums extends React.Component {
  _isMounted = false;

  state = {
    auth: true,
    albums: []
  };

  componentDidUpdate(prevProps) {
    const { query } = this.props;
    if (query !== prevProps.query) {
      this.getAlbums(query);
    }
  }

  componentDidMount = () => {
    this._isMounted = true;
    const { query, albums } = this.props;
    const storedSearch = albums[query];
    if (storedSearch && Object.keys(storedSearch).length) {
      this.setState({
        auth: true,
        loading: false,
        albums: storedSearch
      });
    } else {
      this.getAlbums(query);
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  getAlbums = query => {
    const token = Cookies.get(process.env.AUTH_COOKIE);
    this.setState({ loading: true });

    if (this.props.authGlobalState.auth) {
      search(token, query)
        .then(response => {
          if (response.status >= 200 && response.status < 300) {
            if (this._isMounted) {
              const albums = idx(response, _ => _.data.albums.items);
              this.props.addAlbumsBySlug(query, albums);
              this.setState({
                loading: false,
                auth: true,
                albums: albums
              });
            }
          }
        })
        .catch(() => {
          this.props.invalidate();
          if (this._isMounted) {
            this.setState({
              auth: false
            });
          }
        });
    }
  };

  render() {
    const { query } = this.props;
    const { auth, albums, loading } = this.state;
    return (
      <div>
        {auth && (
          <h3 className={AlbumsStyle.searchResult}>
            {loading
              ? 'Aguarde, carregando... :)'
              : `Resultados encontrados para "${query}"`}
          </h3>
        )}
        {auth ? (
          !loading && <AlbumsList albums={albums} />
        ) : (
          <h3 className={AlbumsStyle.searchResult}>
            Por favor insira um token válido
          </h3>
        )}
      </div>
    );
  }
}

Albums.propTypes = {
  query: PropTypes.string.isRequired,
  authGlobalState: PropTypes.object.isRequired,
  albums: PropTypes.object.isRequired,
  addAlbumsBySlug: PropTypes.func.isRequired,
  invalidate: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  albums: state.searchedAlbums.albums,
  authGlobalState: state.auth
});

const mapDispatchToProps = dispatch => ({
  addAlbumsBySlug: (slug, albums) => dispatch(addAlbumsBySlug(slug, albums)),
  invalidate: () => dispatch(invalidate())
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Albums)
);
