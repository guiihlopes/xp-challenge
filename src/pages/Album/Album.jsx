import React from 'react';
import PropTypes from 'prop-types';
import idx from 'idx';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';
import { addAlbumToStore } from 'store/modules/recentAlbums';
import { invalidate } from 'store/modules/auth';
import AlbumCover from 'components/AlbumCover';
import TrackList from 'components/TrackList';
import { getAlbum } from 'services/spotify/Album';
import AlbumStyle from './Album.css';

class Album extends React.Component {
  _isMounted = false;
  state = {
    auth: true,
    album: {}
  };

  componentDidMount = () => {
    this._isMounted = true;
    const { albumId, albums } = this.props;
    const storedAlbum = albums[albumId];
    if (storedAlbum && Object.keys(storedAlbum).length) {
      this.setState({
        auth: true,
        loading: false,
        album: storedAlbum
      });
    } else {
      this.getAlbumById(albumId);
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  getAlbumById = albumId => {
    const { invalidate, authGlobalState } = this.props;
    const token = Cookies.get(process.env.AUTH_COOKIE);
    this.setState({ loading: true });
    if (authGlobalState.auth) {
      getAlbum(token, albumId)
        .then(response => {
          if (response.status >= 200 && response.status < 300) {
            if (this._isMounted) {
              const album = idx(response, _ => _.data);
              this.props.addAlbumToStore(album);
              this.setState({
                auth: true,
                loading: false,
                album: album
              });
            }
          }
        })
        .catch(() => {
          invalidate();
          if (this._isMounted) {
            this.setState({
              auth: false
            });
          }
        });
    }
  };

  render() {
    const { history } = this.props;
    const { auth, album, loading } = this.state;
    return (
      <React.Fragment>
        <a
          href="#"
          onClick={() => history.goBack()}
          className={AlbumStyle.goBackLink}
        >
          Voltar
        </a>
        <div className={AlbumStyle.album}>
          {auth ? (
            Object.keys(album).length && !loading ? (
              <React.Fragment>
                <div className={AlbumStyle.leftPanel}>
                  <AlbumCover album={album} />
                </div>
                <div className={AlbumStyle.rightPanel}>
                  <TrackList tracks={album.tracks.items} />
                </div>
              </React.Fragment>
            ) : (
              <h3 className={AlbumStyle.message}>Aguarde, carregando... :)</h3>
            )
          ) : (
            <h3 className={AlbumStyle.message}>
              Por favor insira um token válido
            </h3>
          )}
        </div>
      </React.Fragment>
    );
  }
}

Album.propTypes = {
  history: PropTypes.object.isRequired,
  albumId: PropTypes.string.isRequired,
  albums: PropTypes.object.isRequired,
  authGlobalState: PropTypes.object.isRequired,
  addAlbumToStore: PropTypes.func.isRequired,
  invalidate: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  albums: state.recentAlbums.albums,
  authGlobalState: state.auth
});

const mapDispatchToProps = dispatch => ({
  addAlbumToStore: album => dispatch(addAlbumToStore(album)),
  invalidate: () => dispatch(invalidate())
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Album)
);
