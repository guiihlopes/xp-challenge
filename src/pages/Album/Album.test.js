import React from 'react';
import { shallow } from 'enzyme';
import Album from './Album';

describe('Album', () => {
  const AlbumComponent = shallow(<Album />);

  it('renders correctly', () => {
    expect(AlbumComponent).toMatchSnapshot();
  });
});
