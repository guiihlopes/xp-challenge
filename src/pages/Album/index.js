import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./Album'),
  loading: Loading
});

export default class LoadableAlbum extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
