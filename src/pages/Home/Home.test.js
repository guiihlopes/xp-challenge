import React from 'react';
import { shallow } from 'enzyme';
import Home from './Home';

describe('Home', () => {
  const HomeComponent = shallow(<Home />);

  it('renders correctly', () => {
    expect(HomeComponent).toMatchSnapshot();
  });
});
