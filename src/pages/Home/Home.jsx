import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import AlbumsList from 'components/AlbumsList';
import HomeStyle from './Home.css';

class Home extends React.Component {
  render() {
    const recentAlbums = Object.values(this.props.albums).reverse();
    return (
      <section>
        <h3 className={HomeStyle.title}>
          {recentAlbums.length
            ? 'Álbuns buscados recentemente'
            : 'Nenhum álbum buscado recentemente :('}
        </h3>
        <AlbumsList albums={recentAlbums} />
      </section>
    );
  }
}

Home.propTypes = {
  albums: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  albums: state.recentAlbums.albums
});

export default withRouter(connect(mapStateToProps)(Home));
