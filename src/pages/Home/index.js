import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./Home'),
  loading: Loading
});

export default class LoadableHome extends React.Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
