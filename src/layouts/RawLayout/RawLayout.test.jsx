import React from 'react';
import { shallow } from 'enzyme';
import RawLayout from './RawLayout';

describe('RawLayout', () => {
  const RawLayoutComponent = shallow(
    <RawLayout>
      <div>Teste</div>
    </RawLayout>
  );

  it('renders correctly', () => {
    expect(RawLayoutComponent).toMatchSnapshot();
  });
});
