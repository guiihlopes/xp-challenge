import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, matchPath } from 'react-router-dom';
import RawLayoutStyle from './RawLayout.css';
import Header from 'components/Header';
import Album from 'pages/Album';
import NotFoundRoute from 'pages/NotFoundRoute';
import classnames from 'classnames';
import withAuthHandler from 'hoc/withAuthHandler';

class RawLayout extends React.Component {
  render() {
    const { location } = this.props;
    const match = matchPath(location.pathname, {
      path: '/album/:albumId',
      exact: true,
      strict: false
    });

    return (
      <div className={RawLayoutStyle.root}>
        <Header />
        <main className={classnames('container', RawLayoutStyle.main)}>
          <Switch>
            <Route
              exact
              path="/album/:albumId"
              render={() => <Album albumId={match.params.albumId} />}
            />
            <Route component={NotFoundRoute} />
          </Switch>
        </main>
      </div>
    );
  }
}

RawLayout.propTypes = {
  location: PropTypes.object.isRequired
};

export default withAuthHandler(RawLayout);
