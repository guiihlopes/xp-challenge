import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./RawLayout'),
  loading: Loading
});

export default class LoadableRawLayout extends React.Component {
  render() {
    return <LoadableComponent />;
  }
}
