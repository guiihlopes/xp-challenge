import React from 'react';
import { shallow } from 'enzyme';
import { CoreLayout } from './CoreLayout';

describe('CoreLayout', () => {
  const CoreLayoutComponent = shallow(
    <CoreLayout location={{}} history={{ push: jest.fn() }}>
      <div>Teste</div>
    </CoreLayout>
  );

  it('renders correctly', () => {
    expect(CoreLayoutComponent).toMatchSnapshot();
  });

  it('initialize the `state` with an empty input searchString', () => {
    const initialState = {
      searchString: ''
    };

    const state = CoreLayoutComponent.state();
    expect(state.searchString).toEqual(initialState.searchString);
  });

  describe('when the user insert a search string', () => {
    const searchString = 'Mc kevinho';

    beforeEach(() => {
      CoreLayoutComponent.find('#searchQuery').simulate('change', {
        target: { value: searchString }
      });
    });

    it('updates the local inputToken in `state`', () => {
      expect(CoreLayoutComponent.state().searchString).toEqual(searchString);
    });
  });
});
