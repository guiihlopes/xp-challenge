import React from 'react';
import PropTypes from 'prop-types';
import { Route, matchPath } from 'react-router-dom';
import Header from 'components/Header';
import Home from 'pages/Home';
import CoreLayoutStyle from './CoreLayout.css';
import Albums from 'pages/Albums';
import idx from 'idx';
import { stringToSlug, slugToString } from 'helpers/Url';
import classnames from 'classnames';
import withAuthHandler from 'hoc/withAuthHandler';

export class CoreLayout extends React.Component {
  state = {
    searchString: ''
  };

  handleSearch = ev => {
    ev.preventDefault();
    const { history, location } = this.props;
    const { searchString } = this.state;

    if (searchString === '') return false;
    const desiredLocation = `/albums/${stringToSlug(searchString)}`;

    if (location.pathname == desiredLocation) {
      history.go(0);
    }

    history.push(desiredLocation);
  };

  updateSearchString = ev => {
    this.setState({
      searchString: ev.target.value
    });
  };

  getMatchQuery = () => {
    const { location } = this.props;
    return matchPath(location.pathname, {
      path: '/albums/:query',
      exact: true,
      strict: false
    });
  };

  componentDidMount() {
    const match = this.getMatchQuery();
    if (match) {
      this.setState({
        searchString: slugToString(idx(match, _ => _.params.query))
      });
    }
  }

  render() {
    const { searchString } = this.state;

    const match = this.getMatchQuery();

    return (
      <div className={CoreLayoutStyle.root}>
        <Header />
        <main className={classnames('container', CoreLayoutStyle.main)}>
          <form
            action="/albums"
            className={CoreLayoutStyle.searchForm}
            onSubmit={this.handleSearch}
          >
            <div>
              <label className={CoreLayoutStyle.searchLabel}>
                Busque por artistas, álbuns ou músicas
              </label>
              <div className={CoreLayoutStyle.inputContainer}>
                <input
                  type="text"
                  name="searchQuery"
                  id="searchQuery"
                  value={searchString}
                  placeholder="Comece a escrever..."
                  onChange={this.updateSearchString}
                  className={CoreLayoutStyle.searchInput}
                />
              </div>
            </div>
          </form>
          <Route exact path="/" component={Home} />
          <Route
            exact
            path="/albums/:query"
            render={() => (
              <Albums query={slugToString(idx(match, _ => _.params.query))} />
            )}
          />
        </main>
      </div>
    );
  }
}

CoreLayout.propTypes = {
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withAuthHandler(CoreLayout);
