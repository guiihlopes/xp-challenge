import React from 'react';
import Loadable from 'react-loadable';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./CoreLayout'),
  loading: Loading
});

export default class LoadableCoreLayout extends React.Component {
  render() {
    return <LoadableComponent />;
  }
}
