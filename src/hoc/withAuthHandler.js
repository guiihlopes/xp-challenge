import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';
import AccessTokenHandler from 'components/AccessTokenHandler';
import Modal from 'components/Modal';

const withAuthHandler = WrappedComponent => {
  class AuthHandler extends React.Component {
    state = {
      auth: true
    };

    componentDidMount = () => {
      const token = Cookies.get(process.env.AUTH_COOKIE);

      if (!token) {
        this.setState({ auth: false });
      }
    };

    render() {
      const { authGlobalState } = this.props;
      const { auth } = this.state;

      return (
        <React.Fragment>
          <WrappedComponent {...this.props} />
          {(!auth || !authGlobalState.auth) && (
            <Modal>
              <AccessTokenHandler />
            </Modal>
          )}
        </React.Fragment>
      );
    }
  }

  AuthHandler.propTypes = {
    authGlobalState: PropTypes.object.isRequired
  };

  const mapStateToProps = state => ({
    authGlobalState: state.auth
  });

  return withRouter(
    connect(
      mapStateToProps,
      null
    )(AuthHandler)
  );
};

export default withAuthHandler;
