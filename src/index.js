import React from 'react';
import ReactDOM from 'react-dom';
import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import { createStore, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import Root from './router';
import * as reducers from 'store/modules';
import './index.css';

const persistConfig = {
  key: 'root',
  blacklist: ['player'],
  storage
};
const rootReducer = combineReducers(reducers);

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
export const persistor = persistStore(store);

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('./store/modules/index', () => {
    const nextRootReducer = require('./store/modules/index');
    store.replaceReducer(nextRootReducer);
  });
}

ReactDOM.render(
  <Root store={store} persistor={persistor} />,
  document.getElementById('app')
);
