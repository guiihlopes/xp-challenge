# XP Challenge - Mini spotify application

## Check it running on heroku

[XP Challenge](https://xp-challenge.herokuapp.com/)

## Installation

```sh
git clone https://guiihlopes@bitbucket.org/guiihlopes/xp-challenge.git
cd xp-challenge
yarn install
cp .env.example .env
```

## Running :rocket:

### :rocket: Development

```bash
yarn run start # run
```

### :rocket: Production

#### Serve

```bash
yarn install
yarn run build # create dist directory
yarn start:prod # start server
```

#### :rocket: Test

```bash
yarn run test # test
yarn run test:watch
yarn run test:coverage # report coverage
```

## Run with docker (dev mode) :rocket:

```bash
docker-compose build
docker-compose up
```

## How to generate my access token?

#### Using api token to generate a default access token

```bash
curl -X POST \
  https://accounts.spotify.com/api/token \
  -H 'Authorization: Basic  ZDY5MTcyOGRhNzQzNGZkODg5Y2NlZDIwY2JlMjMwZjA6ZjJmZDkxYzQ3YmUyNDczMjg3NjQ1OGNjMDY4ZTIxZTk=' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'cache-control: no-cache' \
  -d 'grant_type=client_credentials'
```

#### Acessing spotify developer console and clicking in "get token" and then get the OAuth Token

[Developer console example](https://developer.spotify.com/console/get-search-item/?q=tania+bowra&type=artist)

## Project structure

    .
    ├── dist                    # Compiled files
    ├── src                     # Source files
    |   ├── components          # Components files
    |   ├── helpers             # Helpers files
    |   ├── hoc                 # High order components
    |   ├── layouts             # Layouts components
    |   ├── pages               # Pages components
    |   ├── router              # Router files
    |   ├── services            # Services files
    |   ├── store               # Redux files
    └── static                  # static files

## Roadmap

- Loadable components improvement
  - Content placeholder
  - Preloading
- Set track in store to prevent multiple request

## Future problems

- How can we know if our stored album list and searched albums has changed?
  - Probably in the future we need websocket to receive updates from spotify api
