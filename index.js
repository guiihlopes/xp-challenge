const express = require('express');
const path = require('path');
const port = parseInt(process.env.PORT, 10) || 3000;

const server = express();

server.use('/static', express.static(path.join(__dirname, 'static')));
server.use(express.static(path.join(__dirname, 'dist')));

server.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

server.listen(port, err => {
  if (err) throw err;
  console.log(`> Ready on http://localhost:${port}`);
});
