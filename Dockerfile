FROM node:10-stretch

WORKDIR /app
COPY . .

COPY .env.example .env

RUN yarn install

RUN yarn build

EXPOSE 3000
CMD ["yarn", "start:prod"]